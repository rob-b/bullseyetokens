# bullseyetokens

Installation
============

Clone this repo then run `stack build --copy-bins`

Example Usage
=============

The best way to use this is to create a file called `tokens.cfg` in the root
of your bullseye-api project (and add `tokens.cfg` to your global gitignore
file) with the following content


```
[live]
email=name@example.com
domain=the.domain.you.login.with.net
url=http://example.com/login
password=password
```

The supported environments are live, stage and local so you create a section
for each environment.

You can then run


```sh
bullseyetokens live
```

and get a new token for the live environment. I typically do

```sh
bullseyetokens live | pbcopy
```

to get a new token and copy it into my osx clipboard.
