{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where
import Network.HTTP.Types
import Network.HTTP.Simple
import Control.Error
import Control.Monad.Except
import Control.Monad.Trans.Either hiding (hoistEither)
import Data.Aeson
import Data.Aeson.Types
import Data.Char (toLower)
import Data.Ini
import Data.Maybe (fromJust)
import System.Exit (exitWith, ExitCode(ExitFailure))
import System.IO
import System.IO.Error
import System.Environment (getArgs)
import GHC.Generics
import qualified Control.Exception as X
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C
import qualified Data.Text as T
import qualified Data.Text.IO as T

data Payload = Payload
  { domain :: T.Text
  , email :: T.Text
  , password :: String
  } deriving (Show, Generic)

data Choices = Choices
  { cDomain :: T.Text
  , cEmail :: T.Text
  , cUrl :: T.Text
  , cPassword :: Maybe T.Text
  } deriving (Show,Generic)

data Token = Token
  { tID :: String
  , tSecret :: String
  } deriving (Show, Generic)

instance ToJSON Payload
instance FromJSON Token where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = drop 1 . map toLower }

type URL = String

getPassword :: IO String
getPassword = do
  putStr "Password: "
  hFlush stdout
  pass <- withEcho False getLine
  putChar '\n'
  return pass

withEcho :: Bool -> IO a -> IO a
withEcho echo action = do
  old <- hGetEcho stdin
  X.bracket_ (hSetEcho stdin echo) (hSetEcho stdin old) action

mkPayload :: Choices -> IO Payload
mkPayload (Choices domain' email' _ password') = Payload domain' email' <$> pass
  where
    pass :: IO String
    pass = maybe getPassword (return . T.unpack) password'

options :: [T.Text]
options = ["live", "stage", "local"]

target :: IO (Maybe T.Text)
target = fmap maybeHead getArgs
  where
    maybeHead :: [String] -> Maybe T.Text
    maybeHead (x:_) = Just (T.pack x)
    maybeHead _ = Nothing

data Yikes e
  = YikesIO e
  | YikesParse e
   deriving (Show)

main :: IO ()
main = do
  target' <- target
  if maybe False (`elem` options) target'
    then do
      _ <- runExceptT (combine "tokens.cfg" (fromJust target'))
      return ()
    else putStrLn "If I had usage instructions I would print them here" >>
         exitWith (ExitFailure 1)

combine :: FilePath -> T.Text -> ExceptT (Yikes T.Text) IO ()
combine fp section = do
    config <- readConfigFile fp `catchE` printError
    config' <- hoistEither (getConfig config) `catchE` printError
    choices <- getConfigChoices' config' section `catchE` printError
    payload' <- lift $ mkPayload choices
    lift $ X.catch (extract (T.unpack $ cUrl choices) payload') statusExceptionHandler >>= C.putStrLn
    return ()

getConfig :: T.Text -> Either (Yikes T.Text) Ini
getConfig configText = either (Left . YikesParse . T.pack) Right (parseIni configText)

printError :: Yikes T.Text -> ExceptT (Yikes T.Text) IO b
printError err' = do
  lift $ case err' of
      YikesIO exc -> T.putStrLn exc
      YikesParse exc -> T.putStrLn (T.append "An error occured while reading the config " exc)
  throwE err'

readConfigFile :: FilePath -> ExceptT (Yikes T.Text) IO T.Text
readConfigFile fp = do
  result <- lift $ safeReadFile fp
  either mkErr return result
  where
    mkErr e = (throwE . YikesIO) $ T.intercalate " " $ map T.pack (errMessage e)
    errMessage :: IOError -> [String]
    errMessage e =
      filter (not . null) [fromMaybe "" (ioeGetFileName e), ioeGetErrorString e]

safeReadFile :: FilePath -> IO (Either X.IOException T.Text)
safeReadFile = X.try . T.readFile

getConfigChoices' :: Monad m => Ini -> T.Text -> ExceptT (Yikes T.Text) m Choices
getConfigChoices' ini section = do
  choices <- getConfigChoices ini section
  hoistEither $ either (Left . YikesParse . T.pack) Right choices

getConfigChoices :: Monad m => Ini -> T.Text -> m (Either String Choices)
getConfigChoices config section =
  runEitherT $
  do d <- either left right (lookupValue' "domain")
     u <- either left right (lookupValue' "url")
     e <- either left right (lookupValue' "email")
     right $ Choices d e u (hush $ lookupValue' "password")
  where
    lookupValue' key = lookupValue section key config

extract :: URL -> Payload -> IO B.ByteString
extract url payload = do
  request' <- parseRequest url
  let request =
        setRequestMethod "POST" .
        setRequestBodyJSON payload . setRequestHeaders [("Accept", "*/*")] $
        request'
  response <- httpJSON request
  let status = getResponseStatus response
      statusCode' = statusCode status
  return $
    case statusCode' of
      201 -> C.pack $ (\t -> tID t ++ " " ++ tSecret t) (getResponseBody response :: Token)
      _ -> statusMessage status

statusExceptionHandler :: HttpException -> IO B.ByteString
statusExceptionHandler e = return . C.pack $ show e
